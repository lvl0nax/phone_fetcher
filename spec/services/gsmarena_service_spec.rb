require 'rails_helper'

RSpec.describe GsmarenaService do
  describe 'CONSTANTS' do
    it 'should have constant BRANDS' do
      expect { described_class::BRANDS }.to_not raise_error
    end

    it 'should have constant SEARCH_URL' do
      expect(described_class::SEARCH_URL).to eql('http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName=')
    end

    it 'should have constant MAIN_URL' do
      expect(described_class::MAIN_URL).to eql('http://www.gsmarena.com/')
    end
  end

  describe 'fetch' do
    it 'phones by brand returns phones hash' do
      html = Nokogiri::HTML(
        '<html>
          <body>
            test
            <div class="makers">
              <ul>
                <li>
                  <a href="test"><strong><span>test_phone</span></strong></a>
                </li>
              </ul>
            </div>
          </body>
        </html>')
      allow_any_instance_of(GsmarenaService).to receive(:html_doc) { html }
      expect(GsmarenaService.new(brand: :apple).fetch_phones_by_brand).to eql('test_phone' => 'test')
    end

    it 'phone returns phone html' do
      html = Nokogiri::HTML(
        '<html>
          <body>
            test
            <div id="specs-list">test html</div>
          </body>
        </html>')
      allow_any_instance_of(GsmarenaService).to receive(:html_doc) { html }
      expect(GsmarenaService.new(id: 'test_url').fetch_phone).to eql('<div id="specs-list">test html</div>')
    end

    it 'phones by query returns phone html without links when result is phone page' do
      html = Nokogiri::HTML(
        '<html>
          <body>
            test
            <div id="specs-list"><a href="test">test html</a></div>
          </body>
        </html>')
      allow_any_instance_of(GsmarenaService).to receive(:html_doc) { html }
      expect(GsmarenaService.new(term: 'test').fetch_phones_by_query).to eql('<div id="specs-list">test html</div>')
    end

    it 'phones by query returns phones list html when result is list of phones' do
      html = Nokogiri::HTML(
        '<html>
          <body>
            test
            <div class="makers">
              <ul>
                <li>
                  <a href="test"><strong><span>test_phone</span></strong></a>
                </li>
              </ul>
            </div>
          </body>
        </html>')
      allow_any_instance_of(GsmarenaService).to receive(:html_doc) { html }
      expect(GsmarenaService.new(term: 'test').fetch_phones_by_query).to eql('<ul><li>test_phone</li></ul>')
    end
  end
end
