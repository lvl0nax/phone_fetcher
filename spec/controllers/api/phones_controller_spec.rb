require 'rails_helper'

RSpec.describe Api::PhonesController, type: :controller do
  describe 'GET index' do
    it 'with term params calls GsmarenaService#fetch_phones_by_query' do
      expect_any_instance_of(GsmarenaService).to receive(:fetch_phones_by_query)
      get :index, term: 'test'
    end

    it 'with brand params calls GsmarenaService#fetch_phones_by_brand' do
      expect_any_instance_of(GsmarenaService).to receive(:fetch_phones_by_brand)
      get :index, brand: 'apple'
    end

    it 'response 400 for empty params' do
      get :index
      expect(response.status).to eq(400)
    end
  end

  describe 'GET show' do
    it 'calls GsmarenaService#fetch_phone' do
      expect_any_instance_of(GsmarenaService).to receive(:fetch_phone)
      get :show, id: 'test'
    end
  end
end
