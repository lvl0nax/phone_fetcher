require 'rails_helper'

RSpec.describe WelcomeController do
  describe 'GET index' do
    it 'assigns @brands' do
      get :index
      expect(assigns(:brands)).to eq([:apple, :samsung, :motorola])
    end

    it 'renders the index template' do
      get :index
      expect(response).to render_template('index')
    end
  end
end
