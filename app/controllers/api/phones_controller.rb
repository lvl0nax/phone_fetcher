class Api::PhonesController < ApplicationController
  def index
    if params[:brand]
      render json: GsmarenaService.new(params).fetch_phones_by_brand
    elsif params[:term]
      render html: GsmarenaService.new(params).fetch_phones_by_query
    else
      render nothing: true, status: 400
    end
  end

  def show
    html = GsmarenaService.new(params).fetch_phone
    render html: html
  end
end
