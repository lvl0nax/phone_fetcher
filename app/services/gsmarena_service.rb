class GsmarenaService
  MAIN_URL = 'http://www.gsmarena.com/'.freeze
  SEARCH_URL = 'http://www.gsmarena.com/results.php3?sQuickSearch=yes&sName='.freeze
  BRANDS = { apple: 'http://www.gsmarena.com/apple-phones-48.php',
             samsung: 'http://www.gsmarena.com/samsung-phones-9.php',
             motorola: 'http://www.gsmarena.com/motorola-phones-4.php' }.freeze

  attr_reader :params

  def initialize(params)
    @params = params
  end

  def fetch_phones_by_brand
    doc = html_doc(BRANDS[params[:brand].to_sym])
    res = parse_phones(doc)
    # IMHO: it works a little bit faster with threads
    process_doc_with_threads(doc.css('.review-nav .nav-pages a')) do |a|
      html = html_doc(a.attributes['href'].to_s)
      res.merge!(parse_phones(html))
    end
    res
  end

  def fetch_phone
    url = params[:id]
    url << '.php' unless url.end_with?('.php')
    doc = html_doc(url).css('#specs-list')
    prepared_html(doc)
  end

  def fetch_phones_by_query
    doc = html_doc(SEARCH_URL + params[:term])
    phone_info = doc.css('#specs-list')
    return prepared_html(phone_info) if phone_info.present?
    html = '<ul>'
    html << finded_phones_html(doc)
    # gsmarena returns only 70 popular items
    # so this is useless code
    # process_doc_with_threads(doc.css('.review-nav .nav-pages a')) do |a|
    #   html << finded_phones_html(html_doc(a.attributes['href'].to_s))
    # end
    html << '</ul>'
  end

  private

  def process_doc_with_threads(elements)
    elements.each_slice(5) do |group|
      threads = []
      group.each do |a|
        threads << Thread.new do
          yield(a)
        end
      end
      threads.each(&:join)
    end
  end

  def prepared_html(doc)
    doc.css('a').each do |elem|
      elem.replace elem.text
    end
    doc.to_html.encode('UTF-8', invalid: :replace, undef: :replace)
  end

  def parse_phones(doc)
    res = {}
    doc.css('.makers ul li a').each do |elem|
      res[elem.text] = elem.attributes['href'].value
    end
    res
  end

  def finded_phones_html(doc)
    res = ''
    doc.css('.makers ul li a').each { |elem| res << "<li>#{elem.text}</li>" }
    res
  end

  def html_doc(url)
    url.prepend(MAIN_URL) unless url.start_with?('http')
    resp = HTTParty.get(url)
    Nokogiri::HTML(resp.response.body)
  end
end
