load_phones = () ->
  brand = $("#brands").val()
  $.ajax
    url: "/api/phones"
    type: 'get'
    dataType: "json"
    data: { brand: brand }
    success: (data)=>
      $('#phones').html('')
      for name, url of data
        $('#phones').append '<option value="' + url + '">' + name + '</option>'
      $('#phones').show()
    error: (data)=>
      alert 'Something went wrong!'

load_phone_info = () ->
  phone = $("#phones").val()
  $.ajax
    url: "/api/phones/" + phone
    type: 'get'
    dataType: "html"
    success: (data)=>
      $('#results').html($('<div />').html(data).text())
    error: (data)=>
      alert 'Something went wrong!'

find_phones = () ->
  term = $(".js_search_field").val()
  if term
    $.ajax
      url: "/api/phones/"
      type: 'get'
      dataType: "html"
      data: { term: term }
      success: (data)=>
        $('#results').html($('<div />').html(data).text())
      error: (data)=>
        alert 'Something went wrong!'
  else
    alert 'Please, fill search field'


$(document).on 'change', '#brands', (e) ->
  load_phones()

$(document).on 'change', '#phones', (e) ->
  load_phone_info()

$(document).on 'click', '.js_search_button', (e) ->
  find_phones()
